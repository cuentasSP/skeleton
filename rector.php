<?php

declare(strict_types=1);

use Rector\Config\RectorConfig;
use Rector\Doctrine\Set\DoctrineSetList;
use Rector\Symfony\Set\SymfonySetList;

/**
 * Configuración para actualizar annotaciones a atributos PHP 8.1
 * @author Walther Bojacá <wbojaca@supresencia.com>
 */
return static function (RectorConfig $rectorConfig): void {
    // Directorios a escanear
    $rectorConfig->paths([
        __DIR__ . '/src/Entity',
        __DIR__ . '/src/Controller',
    ]);

    // Conjunto para convertir anotaciones a atributos en Doctrine
    $rectorConfig->sets([
        DoctrineSetList::ANNOTATIONS_TO_ATTRIBUTES,
    ]);

    // Conjunto para convertir anotaciones a atributos en Symfony
    $rectorConfig->sets([
        SymfonySetList::ANNOTATIONS_TO_ATTRIBUTES,
    ]);
};