<?php
namespace App\Security\Exception;

/**
 * Tuyos, oh Señor, son la grandeza, el poder, la gloria, la victoria y la majestad. 
 * Todo lo que hay en los cielos y en la tierra es tuyo, oh Señor, y este es tu reino. 
 * Te adoramos como el que está por sobre todas las cosas.
 * 1 Cronicas 29:11 NTV
 * 
 * ¡Glorifica al Señor, oh Jerusalén!
 *    ¡Alaba a tu Dios, oh Sion!
 *  Pues él ha reforzado las rejas de tus puertas
 *    y ha bendecido a tus hijos que habitan dentro de tus murallas.
 * Salmo 147:12-13 NTV
 */

use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\Security\Core\Exception\LazyResponseException;

/**
 * Lanza una excepción para redirigir a la url enviada
 * @author Walther Bojaca <wbojaca@supresencia.com>
 */
class RedirectUrlException extends LazyResponseException
{
    public function __construct(string $url, int $status = 302)
    {
        parent::__construct(new RedirectResponse($url, $status));
    }
}