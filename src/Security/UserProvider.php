<?php
namespace App\Security;

/**
 * Pues yo soy el Señor, tu Dios,
 *   el Santo de Israel, tu Salvador.
 * Yo di a Egipto como rescate por tu libertad;
 *   en tu lugar di a Etiopía[a] y a Seba.
 * Entregué a otros a cambio de ti.
 *   Cambié la vida de ellos por la tuya,
 * porque eres muy precioso para mí.
 *   Recibes honra, y yo te amo.
 * Isaías 43:3-4 NTV
 */

use App\Repository\UsersRepository;
use App\Service\Aws\DynamodbAws;
use Sion\ToolsSion;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\Security\Core\Exception\BadCredentialsException;
use Symfony\Component\Security\Core\Exception\UnsupportedUserException;
use Symfony\Component\Security\Core\Exception\UserNotFoundException;
use Symfony\Bundle\SecurityBundle\Security;
use Symfony\Component\Security\Core\User\PasswordAuthenticatedUserInterface;
use Symfony\Component\Security\Core\User\PasswordUpgraderInterface;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\User\UserProviderInterface;

/**
 * Proveedor de usuarios para el microservicio
 * @author Wather Bojacá <wbojaca@supresencia.com>
 */
class UserProvider implements UserProviderInterface, PasswordUpgraderInterface
{
    private $dyn;
    private $ss;
    private $sslog;
    private $security;
    private $userRoleProvider;
    private $repUser;

    const PRF = 'PHPSESSID_';
    const DATAJSON = [
        'loginId',
        'originToken'
    ];

    public function __construct(
        Security $security, 
        UserRoleProvider $userRoleProvider,
        DynamodbAws $dyn, 
        ParameterBagInterface $prms, 
        UsersRepository $repUser
    ) {
        $this->dyn = $dyn;
        $this->security = $security;
        $this->userRoleProvider = $userRoleProvider;
        $this->ss = $prms->get('tabla_sesiones');
        $this->sslog = $prms->get('tabla_sesiones_token');
        $this->repUser = $repUser;
    }

    /**
     * Instancia un objeto UserSession a partir del dataUser del accessToken
     */
    private function bindDataUser(array $dataUser): UserInterface
    {
        $jsonUser = \json_encode(ToolsSion::arrayExtract(self::DATAJSON, $dataUser));
        $user = new UserSession(
            $dataUser['userId'],
            $dataUser['userIdentifier'],
            $this->userRoleProvider->roles($dataUser['userId'], $dataUser['roles']),
            $dataUser['password'],
            $dataUser['salt'],
            $dataUser['lastLogin'],
            $jsonUser
        );
        $user->expires = !empty($dataUser['expires']) ? $dataUser['expires'] : null;
        $user->setOrigin($dataUser);
        $user->setPerson($dataUser);
        return $user;
    } 

    /**
     * Buscar usuario desde la base de datos por es un sp-app
     * Esta era la forma antigua con la que se autentica por el hash de cuentas
     * @author Walther Bojaca <wbojaca@supresencia.com>
     */
    private function getUserHash(string $accessToken): ?UserInterface
    {
        if ($oUser = $this->repUser->userBy(['hash' => $accessToken])) {
            $user = [
                'userId' => $oUser->getId(),
                'userIdentifier' => $oUser->getEmail(),
                'roles' => $oUser->getRoles(),
                'password' => $oUser->getPassword(),
                'salt' => $oUser->getSalt(),
                'lastLogin' => $oUser->getLastLogin()->format("Y-m-d H:i:s"),
                'personId' => $oUser->getIdPersona()->getId(),
                'personHash' => $oUser->getIdPersona()->getHash(),
                'name' => $oUser->getIdPersona()->getNombre(),
                'lastName' => $oUser->getIdPersona()->getApellidos(),
                'gender' => $oUser->getIdPersona()->getGenero(),
                'birth' => $oUser->getIdPersona()->getFechaNacimiento()->format("Y-m-d"),
            ];
            return $this->bindDataUser($user);
        }
        return null;
    }

    /**
     * Obtener el dataUser en dynamo que generó Accounts para crear el UserInterface 
     * Debe existir tanto la sesión del microservicio como la de accounts
     * @author Walther Bojacá <wbojaca@supresencia.com>
     */
    private function getDataUser(string $accessToken): ?UserInterface
    {
        # Evaluar que la sesión exista 
        if (!$this->dyn->getItem($this->ss, ['id' => ['S' => $accessToken]], ['id'])) {
            return null;
        }
        # si existe la sesion continuar con la consulta
        $data = $this->dyn->getItem($this->sslog, ['id' => ['S' => $accessToken]], ['id', 'dataUser', 'expires']);
        if (!empty($data['dataUser']) && ($dataUser = \json_decode($data['dataUser'], true))) {
            $dataUser['expires'] = $data['expires'];
            # Si el dataUser es basico (de un microservicio) buscar el originToken (dataUser de accounts que es el que tiene toda la informacion)
            if (
                !empty($dataUser['originToken']) 
                && empty($dataUser['userId']) 
                && empty($dataUser['personId'])
            ) {
                return $this->getDataUser(self::PRF.$dataUser['originToken']);
            }
            return $this->bindDataUser($dataUser);
        }
        return null;
    }

    /**
     * Obtiene un UserSession apartir de un accessToken
     */
    public function getUser(string $accessToken): ?UserInterface
    {
        $arrayToken = explode('..', $accessToken);
        if (count($arrayToken) == 2 && $arrayToken[0] == 'sp-app') {
            return $this->getUserHash($arrayToken[1]);
        }
        return $this->getDataUser($accessToken);
    }


    /**
     * Symfony calls this method if you use features like switch_user
     * or remember_me.
     *
     * If you're not using these features, you do not need to implement
     * this method.
     *
     * @throws UserNotFoundException if the user is not found
     */
    public function loadUserByIdentifier($identifier): UserInterface
    {
        $user = $this->getUser($identifier);
        if (null === $user && $this->security->getUser()) {
            $user = $this->security->getUser();
        }
        # si no se encuentra el usuario entonces lanzar excepcion 
        if (null === $user) {
            throw new BadCredentialsException('AccessToken invalido');
        }
        return $user;
    }

    /**
     * @deprecated since Symfony 5.3, loadUserByIdentifier() is used instead
     */
    public function loadUserByUsername($username): UserInterface
    {
        return $this->loadUserByIdentifier($username);
    }

    /**
     * Refreshes the user after being reloaded from the session.
     *
     * When a user is logged in, at the beginning of each request, the
     * User object is loaded from the session and then this method is
     * called. Your job is to make sure the user's data is still fresh by,
     * for example, re-querying for fresh User data.
     *
     * If your firewall is "stateless: true" (for a pure API), this
     * method is not called.
     */
    public function refreshUser(UserInterface $user): UserInterface
    {
        if (!$user instanceof UserSession) {
            throw new UnsupportedUserException(sprintf('Invalid user class "%s".', get_class($user)));
        }
        return $user;
    }

    /**
     * Tells Symfony to use this provider for this User class.
     */
    public function supportsClass($class): bool
    {
        return UserSession::class === $class || is_subclass_of($class, UserSession::class);
    }

    /**
     * Upgrades the hashed password of a user, typically for using a better hash algorithm.
     */
    public function upgradePassword(PasswordAuthenticatedUserInterface $user, string $newHashedPassword): void
    {
        //$this->users->update($user, ['hashedPassword' => $newHashedPassword]);
    }

    /**
     * Devuelve el personId si hay un usuario en sesion 
     * @author Walther Bojacá <wbojaca@supresencia.com>
     */
    public static function getPersonId(?UserSession $userSession): ?string
    {
        return $userSession ? $userSession->getPersonId() : null;
    }

    /**
     * Devuelve el personHash si hay un usuario en sesion 
     * @author Walther Bojacá <wbojaca@supresencia.com>
     */
    public static function getPersonHash(?UserSession $userSession): ?string
    {
        return $userSession ? $userSession->getPersonHash() : null;
    }

    /**
     * Devuelve el userId si hay un usuario en sesion 
     * @author Walther Bojacá <wbojaca@supresencia.com>
     */
    public static function getUserId(?UserSession $userSession): ?string
    {
        return $userSession ? $userSession->getId() : null;
    }
}
