<?php
namespace App\Security;

/**
 * Tuyos, oh Señor, son la grandeza, el poder, la gloria, la victoria y la majestad. 
 * Todo lo que hay en los cielos y en la tierra es tuyo, oh Señor, y este es tu reino. 
 * Te adoramos como el que está por sobre todas las cosas.
 * 1 Cronicas 29:11 NTV
 * 
 * te he llamado desde los confines de la tierra,
 *   diciéndote: "Eres mi siervo".
 * Pues te he escogido
 *   y no te desecharé.
 * Isaías 41:9 NTV
 */

use App\Service\Aws\DynamodbAws;
use Sion\ClientSion;
use Sion\DateSion;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;

/**
 * Genrador del AccessToken local para control y manejo de sesión con Accounts
 * @author Walther Bojacá <wbojaca@supresencia.com>
 */
class GeneratorAccessToken
{
    private $dyn;
    private $sslog;
    private $appname;

    public function __construct(DynamodbAws $dyn, ParameterBagInterface $prms)
    {
        $this->dyn = $dyn;
        $this->sslog = $prms->get('tabla_sesiones_token');
        $this->appname = $prms->get('appname');
    }

    /**
     * Devuelve el timestamp de la fecha y hora en que expira
     */
    private static function expire(string $expire = null): string
    {
        $expire = !$expire || DateSion::isDate($expire) ? DateSion::sum('dias', 5, null, true, "Y-m-d") : $expire;
        return DateSion::date($expire)->getTimestamp();
    }

    /**
     * Busca el loginId que creó accounts para registrarlo en el nuevo token
     */
    private static function getLoginId(string $jsonDataUser): string
    {
        $dataUser = \json_decode($jsonDataUser, true);
        return !empty($dataUser['loginId']) ? $dataUser['loginId']: $dataUser['userId'];
    }

    /**
     * Si no existe crea un AccessToken para el UserSession dado
     */
    public function createAccessToken(string $accessToken, UserSession $user): void
    {
        if (!$this->dyn->getItem($this->sslog, ['id' => ['S' => $accessToken]], ['idUsuario'])) {
            $loginId = $user->getLoginId() ?: self::getLoginId($user->getDataUser());
            $expire = $user->expires ?: self::expire();
            $data = [
                'id' =>         ['S' => $accessToken],
                'loginId' =>    ['S' => $loginId],
                'domain' =>     ['S' => $this->appname],
                'ip' =>         ['S' => ClientSion::ipClient()],
                'so' =>         ['S' => ClientSion::SO()],
                'idUsuario' =>  ['N' => ''.$user->getId()],
                'agente' =>     ['S' => ClientSion::userAgent()],
                'navegador' =>  ['S' => ClientSion::browser()],
                'fecha' =>      ['S' => \date("Y-m-d")],
                'signin' =>     ['S' => \date("Y-m-d H:i:s")],
                'estado'=>      ['S' => '103'],
                'dataUser' =>   ['S' => $user->getDataUser()],
                'expires'=>     ['N' => "{$expire}"]
            ];
            if ($user->getPersonId()) {
                $data['idPersona'] = ['N'=> ''.$user->getPersonId()];
            }
            $this->dyn->setItem($this->sslog, $data);
        }
    }
}