<?php
namespace App\Security\Authenticator;

/**
 * Pero tú, Daniel, mantén en secreto esta profecía; sella el libro hasta el tiempo del fin, 
 * cuando muchos correrán de aquí para allá y el conocimiento aumentará.
 * Daniel 12:4 NTV
 */

use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Exception\AuthenticationException;
use Symfony\Component\Security\Core\Exception\CustomUserMessageAuthenticationException;
use Symfony\Component\Security\Http\Authenticator\AbstractAuthenticator;
use Symfony\Component\Security\Http\Authenticator\Passport\Badge\UserBadge;
use Symfony\Component\Security\Http\Authenticator\Passport\Passport;
use Symfony\Component\Security\Http\Authenticator\Passport\SelfValidatingPassport;

/**
 * Autenticador por AccessToken
 * Evalua si puede autenticar una petición por medio de un AccessToken dado
 * @author Walther Bojaca <wbojaca@supresencia.com>
 */
class ApiAccessTokenAuthenticator extends AbstractAuthenticator
{
    private $accessToken;
    private const PRF = 'PHPSESSID_';

    /**
     * Called on every request to decide if this authenticator should be
     * used for the request. Returning `false` will cause this authenticator
     * to be skipped.
     */
    public function supports(Request $request): ?bool
    {
        if ($request->headers->has('X-AUTH-TOKEN')) {
            $this->accessToken = self::PRF.$request->headers->get('X-AUTH-TOKEN');
            return true;
        } elseif ($request->headers->has('SP-APP')) {
            $this->accessToken = 'sp-app..'.$request->headers->get('SP-APP');
            return true;
        }
        return false;
    }

    public function authenticate(Request $request): Passport
    {
        if (null === $this->accessToken) {
            throw new CustomUserMessageAuthenticationException('No API token provided');
        }
        return new SelfValidatingPassport(new UserBadge($this->accessToken));
    }

    public function onAuthenticationSuccess(Request $request, TokenInterface $token, string $firewallName): ?Response
    {
        return null;
    }

    public function onAuthenticationFailure(Request $request, AuthenticationException $exception): ?Response
    {
        $data = [
            'message' => strtr($exception->getMessageKey(), $exception->getMessageData())
        ];
        return new JsonResponse($data, Response::HTTP_UNAUTHORIZED);
    }
}