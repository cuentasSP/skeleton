<?php
namespace App\Security\Authenticator;

/**
 * Pero tú, Daniel, mantén en secreto esta profecía; sella el libro hasta el tiempo del fin, 
 * cuando muchos correrán de aquí para allá y el conocimiento aumentará.
 * Daniel 12:4 NTV
 * 
 * ¡Glorifica al Señor, oh Jerusalén!
 *    ¡Alaba a tu Dios, oh Sion!
 *  Pues él ha reforzado las rejas de tus puertas
 *    y ha bendecido a tus hijos que habitan dentro de tus murallas.
 * Salmo 147:12-13 NTV
 */

use App\Security\Exception\RedirectUrlException;
use App\Security\GeneratorAccessToken;
use App\Security\UserSession;
use App\Service\Aws\DynamodbAws;
use Sion\CryptoSion;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Exception\AuthenticationException;
use Symfony\Component\Security\Core\Exception\CustomUserMessageAuthenticationException;
use Symfony\Bundle\SecurityBundle\Security;
use Symfony\Component\Security\Http\Authenticator\AbstractAuthenticator;
use Symfony\Component\Security\Http\Authenticator\Passport\Badge\UserBadge;
use Symfony\Component\Security\Http\Authenticator\Passport\Passport;
use Symfony\Component\Security\Http\Authenticator\Passport\SelfValidatingPassport;

/**
 * Autenticador por AccessToken
 * Evalua si puede autenticar una petición por medio de un AccessToken dado
 * @author Walther Bojaca <wbojaca@supresencia.com>
 */
class AccessTokenAuthenticator extends AbstractAuthenticator
{
    private $type;
    private $dyn;
    private $keySecret;
    private $tbSessions;
    private $tbSessionsLog;
    private $hostAccounts;
    private $security;
    private $accessToken;
    private $generatorToken;
    private const PRF = 'PHPSESSID_';

    public function __construct(
        Security $security, 
        DynamodbAws $dyn, 
        ParameterBagInterface $prms, 
        GeneratorAccessToken $generatorToken
    ) {
        $this->dyn = $dyn;
        $this->security = $security;
        $this->generatorToken = $generatorToken;
        $this->keySecret = $prms->get('key_secret');
        $this->tbSessions = $prms->get('tabla_sesiones');
        $this->tbSessionsLog = $prms->get('tabla_sesiones_token');
        $this->hostAccounts = $prms->get('host_accounts');
    }

    private function getUserSessionDyn(string $accessToken): ?string
    {
        $data = $this->dyn->getItem($this->tbSessions, ['id' => ['S' => $accessToken]], ['id']);
        if ($data && ($ss = $this->dyn->getItem($this->tbSessionsLog, ['id' => ['S' => $accessToken]], ['idUsuario']))) {
            return $ss['idUsuario'];
        }
        return null;
    }

    /**
     * Evaluar si hay un usuario en sesion y si es el mismo del accessToken
     * Si es el mismo entonces valida si ya se creó su AccessToken local y se lanzan RedirectUrlException
     * @author Walther Bojaca <wbojaca@supresencia.com>
     */
    private function checkAuthenticate(string $accessToken, Request $request)
    {
        if ($this->security->getUser()) {
            if (($userId = $this->getUserSessionDyn($accessToken)) &&  self::getUserId($this->security->getUser()) == $userId) {
                $this->generatorToken->createAccessToken(self::PRF.$request->getSession()->getId(), $this->security->getUser());
                throw new RedirectUrlException($request->getUri());
            }
        }
        return true;
    }

    private static function getUserId(?UserSession $userSession): ?string
    {
        return $userSession ? $userSession->getId() : null;
    }

    /**
     * Called on every request to decide if this authenticator should be
     * used for the request. Returning `false` will cause this authenticator
     * to be skipped.
     */
    public function supports(Request $request): ?bool
    {
        if ($request->headers->has('X-AUTH-TOKEN')) {
            $this->accessToken = self::PRF.$this->getSessionId($request->headers->get('X-AUTH-TOKEN'));
            $this->type = 'X-AUTH-TOKEN';
            return true;
        } elseif ($request->get('acctkn') && $this->checkAuthenticate(self::PRF.$request->get('acctkn'), $request)) {
            $this->accessToken = self::PRF.$this->getSessionId($request->get('acctkn'));
            $this->type = 'acctkn';
            return true;
        }
        return false;
    }

    /**
     * Desencripta el sessionId
     */
    private function getSessionId(string $strBearer): string
    {
        $bearer = explode('.', $strBearer);
        if (count($bearer) == 2) {
            return CryptoSion::decrypt($bearer[0], $bearer[1], $this->keySecret);
        }
        return $strBearer;
    }

    public function authenticate(Request $request): Passport
    {
        if (null === $this->accessToken) {
            throw new CustomUserMessageAuthenticationException('No API token provided');
        }
        return new SelfValidatingPassport(new UserBadge($this->accessToken));
    }

    public function onAuthenticationSuccess(Request $request, TokenInterface $token, string $firewallName): ?Response
    {
        return null;
    }

    public function onAuthenticationFailure(Request $request, AuthenticationException $exception): ?Response
    {
        if ($this->type === 'X-AUTH-TOKEN') {
            $data = [
                'message' => strtr($exception->getMessageKey(), $exception->getMessageData())
            ];
            return new JsonResponse($data, Response::HTTP_UNAUTHORIZED);
        }
        return new RedirectResponse($this->hostAccounts);
    }
}