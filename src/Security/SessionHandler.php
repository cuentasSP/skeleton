<?php
namespace App\Security;

/**
 * Pero tú, Daniel, mantén en secreto esta profecía; sella el libro hasta el tiempo del fin, 
 * cuando muchos correrán de aquí para allá y el conocimiento aumentará.
 * Daniel 12:4 NTV
 */

use Aws\DynamoDb\DynamoDbClient;
use \SessionHandlerInterface;

/**
 * Permite administrar las sesiones en una tabla de AWS-Dynamodb
 * Class SessionHandler
 * Proxy class for \Aws\DynamoDb\Session\SessionHandler which implements SessionHandlerInteface
 * @author Wather Bojacá <wbojaca@supresencia.com>
 */
class SessionHandler implements SessionHandlerInterface 
{

    /**
     * @var \Aws\DynamoDb\DynamoDbClient
     */
    protected $client;

    /**
     * @var \Aws\DynamoDb\Session\SessionHandler
     */
    protected $handler;

    public function __construct(DynamoDbClient $client, array $config) 
    {
        $this->client = $client;
        $this->handler = \Aws\DynamoDb\SessionHandler::fromClient( $this->client, $config);
    }

    /**
     * @return \Aws\DynamoDb\Session\SessionHandler
     */
    public function getHandler() {
        return $this->handler;
    }


    /**
     * Close the session
     */
    public function close(): bool
    {
        return $this->handler->close();
    }

    /**
     * Borrar una session
     * @param string $session_id
     */
    public function destroy($session_id): bool
    {
        return $this->handler->destroy($session_id);
    }

    /**
     * Borrar sesiones antiguas inactivas (garbage collector)
     * @param int $maxlifetime
     */
    public function gc($maxlifetime): int|false
    {
        return $this->handler->gc($maxlifetime);
    }

    /**
     * Inicia una session
     * @param string $save_path
     * @param string $session_id
     */
    public function open($save_path, $session_id): bool
    {
        return $this->handler->open($save_path, $session_id);
    }

    /**
     * Leer los datos de una session
     * @param string $session_id
     */
    public function read($session_id): string
    {
        return $this->handler->read($session_id);
    }

    /**
     * Escribir en la session
     * @param string $session_id
     * @param string $session_data
     */
    public function write($session_id, $session_data): bool
    {
        return $this->handler->write($session_id, $session_data);
    }
}
