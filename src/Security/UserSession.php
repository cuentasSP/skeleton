<?php
namespace App\Security;

/**
 * y el Espíritu Santo, en forma visible, descendió sobre él como una paloma. 
 * Y una voz dijo desde el cielo: «Tú eres mi Hijo muy amado y me das gran gozo».
 * Lucas 3:22 NTV
 */
use Symfony\Component\Security\Core\User\PasswordAuthenticatedUserInterface;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * Objeto usuario de la aplicación
 * @author Walther Bojacá <wbojaca@supresencia.com>
 */
class UserSession implements UserInterface, PasswordAuthenticatedUserInterface
{
    private $id;
    private $loginId;
    private $deviceId;
    private $originToken;
    private $email;
    private $username;
    private $lastLogin;
    private $enabled = true;
    private $roles = [];
    private $password;
    private $salt;
    private $data;

    # informacion de la persona
    private $personId;
    public $personHash;
    public $name;
    public $lastName;
    public $gender;
    public $birth;
    public $perRoles = [];
    public $photo = [];
    public $contactEmail;
    public $phone;
    public $phoneNumber;
    public $nid;
    public $nidCode;
    public $geo = [];
    public $timezone;
    # cuando expira la sesion de accounts
    public $expires;

    public function __construct(
        string $id, 
        string $userIdentifier, 
        array $roles, 
        string $password, 
        string $salt,
        string $lastLogin, 
        string $data
    ) {
        $this->id = $id;
        $this->email = $userIdentifier;
        $this->username = $userIdentifier;
        $this->roles = $roles;
        $this->password = $password;
        $this->salt = $salt;
        $this->lastLogin = $lastLogin;
        $this->data = $data;
    }

    /**
     * Guarda en el usuario los datos de origen de Accounts 
     */
    public function setOrigin(array $data): void
    {
        if (!empty($data['loginId'])) {
            $this->loginId = $data['loginId'];
        }
        if (!empty($data['deviceId'])) {
            $this->deviceId = $data['deviceId'];
        }
        if (!empty($data['originToken'])) {
            $this->originToken = $data['originToken'];
        }
        if (!empty($data['geo'])) {
            $this->geo = $data['geo'];
        }
        if (!empty($data['timezone'])) {
            $this->timezone = $data['timezone'];
        }
    }

    /**
     * Guarda en el usuario los datos de la persona
     */
    public function setPerson(array $data): self
    {
        if (!empty($data['personId'])) {
            $this->personId = $data['personId'];
        }
        if (!empty($data['personHash'])) {
            $this->personHash = $data['personHash'];
        }
        if (!empty($data['name'])) {
            $this->name = $data['name'];
        }
        if (!empty($data['lastName'])) {
            $this->lastName = $data['lastName'];
        }
        if (!empty($data['gender'])) {
            $this->gender = $data['gender'];
        }
        if (!empty($data['birth'])) {
            $this->birth = $data['birth'];
        }
        if (!empty($data['perRoles'])) {
            $this->perRoles = $data['perRoles'];
        }
        if (!empty($data['photo'])) {
            $this->photo = $data['photo'];
        }
        if (!empty($data['contactEmail'])) {
            $this->contactEmail = $data['contactEmail'];
        }
        if (!empty($data['phone'])) {
            $this->phone = $data['phone'];
        }
        if (!empty($data['phoneNumber'])) {
            $this->phoneNumber = $data['phoneNumber'];
        }
        if (!empty($data['nid'])) {
            $this->nid = $data['nid'];
        }
        if (!empty($data['nidCode'])) {
            $this->nidCode = $data['nidCode'];
        }
        return $this;
    }

    public function setId(string $id): self
    {
        $this->id = $id;
        return $this;
    }

    public function getId(): ?string
    {
        return $this->id;
    }

    public function getLoginId(): ?string
    {
        return $this->loginId;
    }

    public function getDeviceId(): ?string
    {
        return $this->deviceId;
    }

    public function getOriginToken(): ?string
    {
        return $this->originToken;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;
        return $this;
    }

    public function setAlternateUsername(string $username): self
    {
        $this->username = $username;
        return $this;
    }

    public function getAlternateUsername(): ?string
    {
        return $this->username;
    }

    public function getEnabled(): ?bool
    {
        return $this->enabled;
    }

    public function setEnabled(bool $enabled): self
    {
        $this->enabled = $enabled;
        return $this;
    }

    public function getLastLogin(): ?string
    {
        return $this->lastLogin;
    }

    public function setLastLogin(string $lastLogin): self
    {
        $this->lastLogin = $lastLogin;
        return $this;
    }

    public function setPersonId(string $personId): self
    {
        $this->personId = $personId;
        return $this;
    }

    public function getPersonId(): ?string
    {
        return $this->personId;
    }

    public function getPersonHash(): ?string
    {
        return $this->personHash;
    }

    /**
     * A visual identifier that represents this user.
     *
     * @see UserInterface
     */
    public function getUserIdentifier(): string
    {
        return (string) $this->email;
    }

    /**
     * @deprecated since Symfony 5.3, use getUserIdentifier instead
     */
    public function getUsername(): string
    {
        return (string) $this->email;
    }

    /**
     * @see UserInterface
     */
    public function getRoles(): array
    {
        $roles = $this->roles;
        // guarantee every user at least has ROLE_USER
        $roles[] = 'ROLE_USER';

        return array_unique($roles);
    }

    public function setRoles(array $roles): self
    {
        $this->roles = $roles;
        return $this;
    }

    /**
     * @see PasswordAuthenticatedUserInterface
     */
    public function getPassword(): string
    {
        return $this->password;
    }

    public function setPassword(string $password): self
    {
        $this->password = $password;
        return $this;
    }

    /**
     * Returning a salt is only needed, if you are not using a modern
     * hashing algorithm (e.g. bcrypt or sodium) in your security.yaml.
     *
     * @see UserInterface
     */
    public function getSalt(): ?string
    {
        return $this->salt;
    }

    public function getDataUser(): ?string
    {
        return $this->data;
    }

    /**
     * @see UserInterface
     */
    public function eraseCredentials(): void
    {
        // If you store any temporary, sensitive data on the user, clear it here
        // $this->plainPassword = null;
    }
}
