<?php

namespace App\Security;

/**
 * Tuyos, oh Señor, son la grandeza, el poder, la gloria, la victoria y la majestad. 
 * Todo lo que hay en los cielos y en la tierra es tuyo, oh Señor, y este es tu reino. 
 * Te adoramos como el que está por sobre todas las cosas.
 * 1 Cronicas 29:11 NTV
 * 
 * ¡Glorifica al Señor, oh Jerusalén!
 *    ¡Alaba a tu Dios, oh Sion!
 *  Pues él ha reforzado las rejas de tus puertas
 *    y ha bendecido a tus hijos que habitan dentro de tus murallas.
 * Salmo 147:12-13 NTV
 * 
 * Definición de las rutas que requieren autorización para acceder o para realizar una acción
 * id: Es parte de prefijo que tendra el codigo de autorización. el código completo es: {nameapp}.{id}_{sessionId}
 * type: Es el mecanismo que usará accounts para validar la identidad, solo existen dos (password o code)
 * ttl: Es el tiempo de vida de la autorización en minutos (el borrado es apróximado y depende de dynamodb)
 * trustLevel: Es opcional y se envía para indicar si el nivel de confianza (bajo, medio, alto) es igual al enviado entonces se genera la autorizacion sin necesidad de validar tipo (type)
 * blockLoginAs: Es opcional, cuando se envia (true) indica que se valida el usuario sin importar si es un loginAs
 * 
 * @author Walther Bojaca <wbojaca@supresencia.com>
 */
class ProtectedRoutes
{
    /**
     * Aqui las rutas de acceso (GET) protegidas con una autorización
     */
    const _PROTECTED_ACCESS = [
        'app_test' => ['id' => 'auth', 'type' => 'code', 'ttl' => 10, 'trustLevel' => 'alto', 'blockLoginAs' => true],
    ];

    /**
     * Aqui las rutas de acción (POST) protegidas con una autorización
     */
    const _PROTECTED_ACTION = [
        //'app_test_form' => ['id' => 'auth', 'type' => 'code', 'ttl' => 10],
    ];
}