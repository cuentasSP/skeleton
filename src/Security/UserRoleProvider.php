<?php
namespace App\Security;

/**
 * Pues el Señor declara: «He puesto a mi rey elegido en el trono de Jerusalén, en mi monte santo».
 * Salmo 2:6 NTV
 */

use Sion\SqlSion;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;

/**
 * Servicio para caragar todos los roles que puede tener una persona 
 * de acuerdo la informacion de cta_per_roles y grls_roles
 * @author Walther Bojacá <wbojaca@supresencia.com>
 */
class UserRoleProvider
{
    private $sql;
    private $appname;

    public function __construct(SqlSion $sql, ParameterBagInterface $prms) 
    {
        $this->sql = $sql;
        $this->appname = $prms->get('appname');
    }

    /**
     * Devuelve todos los roles que puede tener una persona a partir de los 
     * que ya tiene asignados en el sistema mas los que puede tener por cta_per_roles
     */
    public function roles(string $userId, array $systemRoles = []): array
    {
        $roles = \array_flip($systemRoles);
        foreach ($this->sql->fetchAll(self::roleQuery(['userId' => $userId], $this->appname), 'readonly') as $itm) {
            $roles[$itm['rol']] = $itm['rol'];
        }
        return \array_keys($roles);
    }

    /**
     * Devuelve un strin WHERE para ubicar una persona por algún dato único
     */
    public static function where(array $by): string
    {
        $where = '';
        if (!empty($by['personId'])) {
            $where = "WHERE cp.id = '{$by['personId']}'";
        } elseif (!empty($by['personHash'])) {
            $where = "WHERE cp.hash = '{$by['personHash']}'";
        } elseif (!empty($by['userId'])) {
            $where = "WHERE au.id = '{$by['userId']}'";
        }
        return $where;
    }

    /**
     * Query para obtener los roles de una persona
     */
    public static function roleQuery(array $by, string $appname): string
    {
        $where = self::where($by);
        return $where ? "SELECT gr.rol 
                            FROM cta_personas cp
                            INNER JOIN acc_usuarios au ON au.id_persona = cp.id
                            INNER JOIN cta_per_roles cpr ON au.id_persona = cpr.id_persona
                            INNER JOIN grls_servicios_roles gsr ON gsr.id_tipo = cpr.id_tipo_perfil AND gsr.estado_tipo = cpr.estado AND gsr.estado = '101'
                            INNER JOIN grls_roles gr ON gr.rol = gsr.rol AND gr.estado = '101'
                            INNER JOIN grls_servicios gs ON gs.codigo = gsr.servicio AND gs.codigo = '{$appname}'
                            {$where}" :  '';
    }
}