<?php
namespace App\Service\Api;

/**
 * Vendrá gente de muchas naciones y dirán: 
 * «Vengan, subamos al monte del Señor, a la casa del Dios de Jacob. 
 * Allí él nos enseñará sus caminos, y andaremos en sus sendas». 
 * Pues de Sión saldrá la enseñanza del Señor; de Jerusalén saldrá su palabra.
 * Isaías 2:3
 */

use Sion\AbstractApiClientSion;

/**
 * Cliente Rest para conectar con la API de elsp
 * @author Walther Bojacá <wbojaca@supresencia.com>
 */
class AccountsApi extends AbstractApiClientSion
{
    protected function setToken(): void
    {
        $this->token = $this->parameter->get('api_elsp_token');
    }

    protected function getHost(): string
    {
        return $this->parameter->get('host_accounts').'/api';
    }
}