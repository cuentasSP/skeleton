<?php
namespace App\Service\Api;

/**
 * El cielo y la tierra desaparecerán, pero mis palabras no desaparecerán jamás.
 * Mateo 24:35 NTV
 */

use Sion\AbstractApiClientSion;

/**
 * Cliente Rest para conectar con la API de elsp
 * @author Walther Bojacá <wbojaca@supresencia.com>
 */
class ElspApi extends AbstractApiClientSion
{
    protected function setToken(): void
    {
        $this->token = $this->parameter->get('api_elsp_token');
    }

    protected function getHost(): string
    {
        return $this->parameter->get('api_elsp_path').'/api';
    }
}