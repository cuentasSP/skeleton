<?php
namespace App\Service\Aws;

/**
 * ¡Cuán preciosa, oh Dios, es tu misericordia!
 * Por eso los hijos de los hombres se amparan bajo la sombra de tus alas.
 *  Serán completamente saciados de la grosura de tu casa,
 * Y tú los abrevarás del torrente de tus delicias.
 *  Porque contigo está el manantial de la vida;
 * En tu luz veremos la luz.
 *  Extiende tu misericordia a los que te conocen,
 * Y tu justicia a los rectos de corazón.
 * Salmo 36:7-10 RVR
 * 
 * Permite generar las conexiones y creación de clientes hacia los diferentes servicios de AWS
 * @author Walther Bojacá <wbojaca@supresencia.com>
 */

abstract class AbstractAws
{
    protected $client;
    protected $conParameters = [];
    public $error;

    /**
     * Implementar la creación  del cliente
     */
    abstract public function setClient(string $name): void;

    /**
     * Agrega parametros de conexión para crear clientes que se conecten al servicio de AWS
     */
    public function addConnectionParameter(
        string $name, 
        string $key, 
        string $secret, 
        string $region,
        string $version = 'latest'
    ): void {
        $this->conParameters[$name] = [
            'credentials' => [
                'key' => $key,
                'secret' => $secret
            ],
            'region' => $region,
            'version' => $version
        ];
    }

    /**
     * Devolver el cliente implementado
     */
    public function getClient(string $name = 'default')
    {
        $this->setClient($name);
        return $this->client;
    }
}
