<?php
namespace App\Service\Aws;

/**
 * Entonces ustedes sabrán que yo, el Señor su Dios,
 * habito en Sión, mi monte santo.
 * Jerusalén será santa para siempre
 * y los ejércitos extranjeros nunca más volverán a conquistarla.
 * En aquel día las montañas destilarán vino dulce
 * y de los montes fluirá leche.
 * El agua llenará los arroyos de Judá
 * y del templo del Señor brotará una fuente
 * que regará el árido valle de las acacias.
 * Joel 3:17-18 NTV
 */

use Aws\DynamoDb\DynamoDbClient;
use Aws\DynamoDb\Exception\DynamoDbException;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;

/**
 * Permite trabajar con Dynamodb
 * @author Walther Bojacá <wbojaca@supresencia.com>
 */
class DynamodbAws extends AbstractAws
{
    
    public function __construct(ParameterBagInterface $prms)
    {
        $this->addConnectionParameter(
            'default', 
            $prms->get('aws_dynamo_key'), 
            $prms->get('aws_dynamo_secret'),
            $prms->get('aws_dynamo_region'),
            $prms->get('aws_version')
        );
        $this->setClient('default');
    }

    /**
     * Crea una instancia DynamoDbClient
     */
    public function setClient(string $name = 'default'): void
    {
        if (!empty($this->conParameters[$name])) {
            $this->client = new DynamoDbClient($this->conParameters[$name]);
        } 
    }

    public static function parameterQuery(array $keys): array
    {
        $filter = [];
        foreach ($keys as $key => $itm) {
            $operator = !empty($itm['operator']) ? $itm['operator'] : 'EQ';
            $type = !empty($itm['type']) ? $itm['type'] : 'S';
            $filter[$key] = ['AttributeValueList' => [[$type => $itm['value']]], 'ComparisonOperator' => $operator];
        }
        return $filter;
    }

    private function format(array $item, array $labels = null): array
    {
        if(is_array($labels)){
            $aItem = [];
            foreach($labels as $lab){
                if(isset($item[$lab])){
                    $val = array_values($item[$lab]);
                    $aItem[$lab] = $val[0];
                }
            }
            return $aItem;
        }
        return $item;
    }

    /**
     * Devuelve objeto iterador con los resuldaos de un Query sobre los keys de la tabla
     * Formato keyFilros: array('nombre_key'=>array('AttributeValueList'=>array(array('tipoValor'=>valor)),'ComparisonOperator' => 'EQ'));
     */
    public function query(string $table, array $keyFiltros, string $indexName = null)
    {
        $query = [
            'TableName' => $table,
            'KeyConditions' => $keyFiltros
        ];
        if($indexName){
            $query = array_merge($query,['IndexName' => $indexName]);
        }
        return $this->getClient()->getIterator('Query', $query); 
    }
    
    public function scan(string $table, array $filtros)
    {
        return $this->getClient()->getIterator('Scan', [
            'TableName'=>$table,
            'ScanFilter' => $filtros
        ]); 
    }

    /**
     * Devuelve un item de una tabla o falso si no se encontro 
     */
    public function getItem(string $table, array $key, array $labels = null): array
    {
        $data = $this->getClient()->getItem([
            'ConsistentRead' => true,
            'TableName' => $table,
            'Key'=> $key
        ]);
        if(!empty($data['Item'])){ 
            return $this->format($data['Item'], $labels);
        }
        return [];
    }

    /**
     * Devuelve los items para los filtros de busqueda con la funcion Query
     */
    public function getItems(string $table, array $keyFil, array $labels = null, string $indexName = null): array
    {
        $data = [];
        $iterador = $this->query($table, $keyFil, $indexName);
        foreach($iterador as $item){ 
            $data[] = $this->format($item, $labels); 
        }
        return $data;
    }

    public function scanItems(string $table, array $keyFil, array $labels = null): array
    {
        $data = [];
        $iterador = $this->scan($table, $keyFil);
        foreach($iterador as $item){ 
            $data[] = $this->format($item, $labels); 
        }
        return $data;
    }

    /**
     * Agrega un nuevo item en una tabla de dynamodb
     */
    public function setItem(string $table, array $data): void
    {
        $this->getClient()->putItem([
            'TableName' => $table,
            'Item' => $data
        ]); 
    }

    /**
     * Actualiza un item
     */
    public function updateItem(string $table, array $keys, array $data): void
    {
        $this->getClient()->updateItem([
            'TableName' => $table,
            'Key' => $keys,
            'AttributeUpdates' => $data
        ]); 
    }

    /**
     * Elimina un item de una tabla
     */
    public function removeItem(string $table, array $keys): bool
    {
        try {
            $this->getClient()->deleteItem([
                'TableName' => $table,
                'Key' => $keys
            ]);
            return true;
        }catch (DynamoDbException $e) {
            $this->error = ['estado' => false, 'mensaje' => $e->getMessage()];
            return false;
        }
    }

    /**
     * Devuelve el numero de registros para un query
     * Ejemplo: $keyFil = [
     *       'app' => [
     *           'AttributeValueList' => [['S'=>'serviceNameLog']],'ComparisonOperator' => 'EQ'
     *       ]
     *   ];
     */
    public function count(string $table, array $keyFil, string $indexName): ?string
    {
        return \iterator_count($this->query($table, $keyFil, $indexName));
    }

}