<?php
namespace App\Service\Aws;

/**
 * ¡Cuán preciosa, oh Dios, es tu misericordia!
 * Por eso los hijos de los hombres se amparan bajo la sombra de tus alas.
 *  Serán completamente saciados de la grosura de tu casa,
 * Y tú los abrevarás del torrente de tus delicias.
 *  Porque contigo está el manantial de la vida;
 * En tu luz veremos la luz.
 *  Extiende tu misericordia a los que te conocen,
 * Y tu justicia a los rectos de corazón.
 * Salmo 36:7-10 RVR
 */

use Aws\Result;
use Aws\S3\S3Client;
use Aws\S3\Exception\S3Exception;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;

/**
 * Permite trabajar con S3 de AWS
 * @author Walther Bojacá <wbojaca@supresencia.com>
 */
class S3Aws extends AbstractAws
{
    
    public function __construct(ParameterBagInterface $prms)
    {
        $this->addConnectionParameter(
            'default', 
            $prms->get('aws_s3_key'), 
            $prms->get('aws_s3_secret'),
            $prms->get('aws_s3_region'),
            $prms->get('aws_version')
        );
        $this->setClient('default');
    }

    /**
     * Crear una instancia S3Client
     */
    public function setClient(string $name = 'default'): void
    {
        if (!empty($this->conParameters[$name])) {
            $this->client = new S3Client($this->conParameters[$name]);
        }
    }

    /**
     * Devuelve una url con acceso temporal a un objeto en S3
     */
    public function getObjectUrl(string $bucket, string $key, string $expire = '+5 minutes'): string
    {
        $cmd = $this->getClient()->getCommand('GetObject', [
            'Bucket' => $bucket,
            'Key' => $key
        ]);
        $request = $this->getClient()->createPresignedRequest($cmd, $expire);
        return (string) $request->getUri();
    }

    /**
     * Crear objeto en S3 a partir de un $fileContent, opcional se puede enviar el $contentType
     * $fileCOntent es un string representación de archivo por lo general viene de la función file_get_contents de PHP
     */
    public function uploadFileContent(string $bucket, string $key, string $fileContent, string $contentType = null): Result
    {
        $aCotentType = $contentType ? ['ContentType' => $contentType] : [];
        return $this->getClient()->putObject(\array_merge(
            [
                'Bucket'      => $bucket,
                'Key'         => $key,
                'Body'        => $fileContent
            ], $aCotentType
        ));
    }

    /**
     * Sube a S3 el $sourceFile (ruta del archivo)
     */
    public function uploadSourceFile(string $bucket, string $key, string $sourceFile): Result
    {
        return $this->getClient()->putObject([
            'Bucket'     => $bucket,
            'Key'        => $key,
            'SourceFile' => $sourceFile,
        ]);
    }

    /**
     * Evalua si el objeto $key existe
     */
    public function objectExists(string $bucket, string $key): bool
    {
        return $this->getClient()->doesObjectExist($bucket, $key);
    }

    /**
     * Elimina un objeto de S3
     */
    public function deleteObject(string $bucket, string $key): Result
    {
        return $this->getClient()->deleteObject([
            'Bucket' => $bucket,
            'Key'    => $key
        ]);
    }
}