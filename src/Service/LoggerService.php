<?php
namespace App\Service;

use App\Service\Aws\DynamodbAws;
use Sion\DateSion;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;

/**
 * Servicio para el registro de logs en dynamodb
 * @author Walther Bojacá <wbojaca@supresencia.com>
 */
class LoggerService
{
    private $tbLogger;
    private $dynamodbAws;

    public function __construct(ParameterBagInterface $prms, DynamodbAws $dynamodbAws)
    {
        $this->tbLogger = $prms->get('dynamo_logger');
        $this->dynamodbAws = $dynamodbAws;
    }

    /**
     * Devuelve un string con la fecha y hora en que un dato expira
     */
    public function expire(\DateTime $expire = null): string
    {
        return $expire ? $expire->getTimestamp() : DateSion::sum('meses', '1', null);
    }

    /**
     * Para registrar en el log de dynamo un evento
     */
    public function register(
        string $userId,
        string $sessionId,
        string $appname, 
        string $topic,
        array $logData,
        string $status, 
        \DateTime $expire = null
    ): void {
        $timestamp = $this->expire($expire);
        $date = DateSion::date();
        $sessionId = !empty($sessionId) ? $sessionId : 'api';
        $this->dynamodbAws->setItem($this->tbLogger, [
            'id'=>          ['S'=> \uniqid()],
            'userId'=>      ['S'=> "{$userId}"],
            'sessionId'=>   ['S'=> $sessionId],
            'app'=>         ['S'=> $appname],
            'topic' =>      ['S'=> $topic],
            'data'=>        ['S'=> \json_encode($logData)],
            'status'=>      ['S'=> "$status"],
            'dateTime'=>    ['S'=> $date->format("Y-m-d H:i:s")],
            'date'=>        ['S'=> $date->format("Y-m-d")],
            'expira'=>      ['N'=> "$timestamp"]
        ]);
    }
}