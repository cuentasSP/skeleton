<?php
namespace App\Service\Person;

use App\Service\Query\PersonQuery;
use Sion\SqlSion;

class GetPerson
{
    private $sql;

    public function __construct(SqlSion $sql)
    {
        $this->sql = $sql;
    }

    public function person(array $by): array
    {
        return $this->sql->fetchOne(PersonQuery::getPerson($by));
    }
}
