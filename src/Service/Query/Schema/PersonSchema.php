<?php
namespace App\Service\Query\Schema;

class PersonSchema
{
    const SCHEMA = [
        'person' => [
            'cls' => "cp.id personId, cp.codigo personCode, cp.hash personHash, cp.fecha createdAt, cp.hash_bio bio, cp.nombre name, cp.apellidos lastName, cp.genero gender, IF(cp.genero='F','Mujer','Hombre') genderName, cp.fecha_nacimiento birth, (YEAR(CURDATE())-YEAR(cp.fecha_nacimiento))-(RIGHT(CURDATE(),5)<RIGHT(cp.fecha_nacimiento,5)) age, cp.editar isEditable, cp.nivel accessLevel",
            'tbs' => 'cta_personas cp'
        ],
        'status' => [
            'cls' => 'cpe.id personStatusId, cpe.descripcion personStatusName',
            'tbs' => 'grls_estados cpe ON cpe.id = cp.estado'
        ],
        'origin' => [
            'cls' => "cpo.id personCountryId, cpo.pais personCountryName",
            'tbs' => "grls_paises cpo ON cpo.id = cp.pais_origen"
        ],
        'user' => [
            'cls' => "cpus.id userId, cpus.hash userHash, cpus.username_canonical username, cpus.email_canonical userIdentifier, cpus.last_login lastLogin, cpus.roles userRoles, cpus.enabled userEnabled",
            'tbs' => "acc_usuarios cpus ON cpus.id = cp.usuario"
        ],
        'userApp' => [
            'cls' => "cpusa.nick nickname, cpusa.picture",
            'tbs' => "acc_usuarios_apps cpusa ON cpusa.usuario = cpus.id"
        ]
    ];

    # Receta Minima
    const BASIC = [
        'inner' => ['status', 'origin']
    ];

    # Receta por defecto
    const DEFAULT = [
        'inner' => ['status', 'origin'],
        'left'  => ['user', 'userApp'],
    ];
}