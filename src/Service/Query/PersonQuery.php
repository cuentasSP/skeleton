<?php
namespace App\Service\Query;

use App\Service\Query\Schema\PersonSchema;
use Sion\SqlSion;

class PersonQuery
{
    const FILTERSWHERE = [
        'personId' => 'cp.id',
        'personHash' => 'cp.hash',
        'personCode' => 'cp.codigo',
    ];

    /**
     * Devuelve el array de acuerdo a los filters, queries y parametros dados
     */
    public static function getQuery(
        array $filters = [], 
        array $recipe = null, 
        array $default = null,
        string $parameters = null
    ): array {
        $recipe = $recipe ?: $default;
        $recipe['where'] = SqlSion::where(self::FILTERSWHERE, $filters);
        if (!empty($parameters)) {
            $recipe['prm'] = $parameters;
        }
        return $recipe;
    }

    public static function getPerson(array $by, array $recipe = null): array
    {
        return SqlSion::buildQuery(PersonSchema::SCHEMA, PersonSchema::SCHEMA['person'],
            self::getQuery($by, $recipe, PersonSchema::DEFAULT)
        );
    }
}