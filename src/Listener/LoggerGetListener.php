<?php
namespace App\Listener;

/**
 * Pues todo lo puedo hacer por medio de Cristo, quien me da las fuerzas.
 * Filipenses 4:13 NTV
 */

use App\Security\UserSession;
use App\Service\LoggerService;
use Sion\ClientSion;
use Sion\DateSion;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Bundle\SecurityBundle\Security;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Event\RequestEvent;

/**
 * Servicio para registrar el historico de navegación
 * @author Walther Bojacá <wbojaca@supresencia.com>
 */
class LoggerGetListener
{
    private $security;
    private $logger;
    private $appname;

    const ROUTE = [
        'app_test',
    ];

    public function __construct(
        Security $security, 
        LoggerService $logger,
        ParameterBagInterface $prms
    ) {
        $this->security = $security;
        $this->logger = $logger;
        $this->appname = $prms->get('appname');
    }

    public function onKernelRequest(RequestEvent $event): void
    {
        $request = $event->getRequest();
        $route = $request->attributes->get('_route');
        if (
            $request->getMethod() == 'GET'
            && in_array($route, static::ROUTE)
            && ($user = $this->security->getUser()) 
        ) {
            $data = $this->getDataRequest($request, $user);
            $this->logger->register(
                $data['userId'],
                $request->getSession()->getId(), 
                $this->appname, 
                'access', 
                $data, 
                '103', 
                DateSion::sum('day', 7)
            );
        }
    }

    private function getDataRequest(Request $request, UserSession $user): array
    {
        $personId = $user->getPersonId() ? $user->getPersonId() : '';
        return [
            'userId' => $user->getId(),
            'personId' => $personId,
            'ip' => ClientSion::ipClient(),
            'route' => $request->attributes->get('_route'),
            'method' => $request->getMethod(),
            'url' => $request->getPathInfo()
        ];
    }

}