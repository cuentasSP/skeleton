<?php
namespace App\Listener;

/**
 * Pues todo lo puedo hacer por medio de Cristo, quien me da las fuerzas.
 * Filipenses 4:13 NTV
 */

use App\Security\UserSession;
use App\Service\Aws\DynamodbAws;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\HttpKernel\Event\RequestEvent;
use Symfony\Bundle\SecurityBundle\Security;

/**
 * Servicio para sincronizar el expire de la sesion y dejarlo igual al sesion de accounts
 * @author Walther Bojacá <wbojaca@supresencia.com>
 */
class ExpireSessionListener
{
    private $security;
    private $dyn;
    private $tbSessions;


    public function __construct(
        Security $security,
        DynamodbAws $dyn, 
        ParameterBagInterface $prms
    ) {
        $this->dyn = $dyn;
        $this->security = $security;
        $this->tbSessions = $prms->get('tabla_sesiones');
    }

    public function onKernelRequest(RequestEvent $event): void
    {
        $request = $event->getRequest();
        if ($request->getSession()->get('_syncSessionExpire') && ($user = $this->security->getUser())) {
            $this->syncExpireSession($request->getSession()->getId(), $user);
            $request->getSession()->remove('_syncSessionExpire');
        }   
    }

    /**
     * Deja la sesion de la aplicación con el mismo expires de la sesion de accounts
     */
    private function syncExpireSession(string $sessionId, UserSession $user): void
    {
        $sessionId = 'PHPSESSID_'.$sessionId;
        $accountsSessionId = 'PHPSESSID_'.$user->getOriginToken();
        $sessionMain = $this->dyn->getItem($this->tbSessions, ['id' => ['S' => $accountsSessionId]], ['expires']);
        if (!empty($sessionMain['expires'])) {
            $key = ['id' => ['S' => $sessionId]];
            $data = [
                'expires' => ['Value' => ['N' => $sessionMain['expires']], 'Action' => 'PUT']
            ];
            $this->dyn->updateItem($this->tbSessions, $key, $data);
        }
    }
}