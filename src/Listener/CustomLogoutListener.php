<?php
namespace App\Listener;

/**
 * Ciertamente, yo soy la vid; ustedes son las ramas. 
 * Los que permanecen en mí y yo en ellos producirán mucho fruto porque, 
 * separados de mí, no pueden hacer nada. 
 * Juan 15:5 NTV
 */

use App\Security\UserSession;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\Security\Http\Event\LogoutEvent;

/**
 * Redirige a accounts para cerrar todas las sesiones activas
 * @author Walther Bojaca <wbojaca@supresencia.com>
 */
class CustomLogoutListener
{
    private $hostAccountsLogout;

    public function __construct(ParameterBagInterface $prms)
    {
        $this->hostAccountsLogout = $prms->get('host_accounts').'/logout';
    }

    public function onSymfonyComponentSecurityHttpEventLogoutEvent(LogoutEvent $logoutEvent): void
    {
        if ($logoutEvent->getToken()) {
            # enviamos la informacion del loginId y usuario para cerrar sesion
            $bearer = $this->getBearer($logoutEvent->getToken()->getUser());
            $logoutEvent->setResponse(new RedirectResponse($this->hostAccountsLogout."?outss={$bearer}"));
        }
    }

    private function getBearer(UserSession $userSession): string
    {
        return \uniqid() .'.' . $userSession->getId();
    }
}