<?php
namespace App\Listener;

/**
 * Pues todo lo puedo hacer por medio de Cristo, quien me da las fuerzas.
 * Filipenses 4:13 NTV
 */

use App\Security\UserSession;
use App\Service\LoggerService;
use Doctrine\Common\EventSubscriber;
use Doctrine\ORM\Events;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\Persistence\Event\LifecycleEventArgs;
use Sion\ClientSion;
use Sion\DateSion;
use Sion\SqlSion;
use Symfony\Bundle\SecurityBundle\Security;
use Symfony\Component\Console\Event\ConsoleCommandEvent;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * Evento que está escuchando cuando se realizaron cambios en la base de datos (insert, update, delete) 
 * Se registra en en la tabla de logs 
 * @author Walther Bojacá <wbojaca@supresencia.com>
 */
class LoggerActionsListener implements EventSubscriber
{
    const LISTENTITIES = [
        'App\Entity\AccUsuariosReset', 
    ];

    const EXCLUDEROUTES = [
        'app_continue_action_user',
    ];

    private $sqlSion;
    private $security;
    private $requestStack;
    private $entityManager;
    private $logger;
    private $appname;
    private $strategy;

    public function __construct(
        SqlSion $sqlSion,
        Security $security, 
        RequestStack $requestStack,
        EntityManagerInterface $entityManager,
        LoggerService $logger,
        ParameterBagInterface $prms
    ) {
        $this->sqlSion = $sqlSion;
        $this->security = $security;
        $this->requestStack = $requestStack;
        $this->entityManager = $entityManager;
        $this->logger = $logger;
        $this->appname = $prms->get('appname');
        $this->strategy = $prms->get('logger_filter_strategy');
    }

    public function getSubscribedEvents(): array
    {
        return [
            Events::postPersist,
            Events::postUpdate,
            Events::postRemove,
            'console.command'
        ];
    }

    public function postPersist(LifecycleEventArgs $args)
    {
        $this->logChange('insert', $args);
    }

    public function postUpdate(LifecycleEventArgs $args)
    {
        $this->logChange('update', $args);
    }

    public function postRemove(LifecycleEventArgs $args)
    {
        $this->logChange('delete', $args);
    }

    public function onConsoleCommand(ConsoleCommandEvent $event)
    {
        //Operación de base de datos ejecutada por un comando, omitiendo el registro.;
    }

    private function logChange($action, LifecycleEventArgs $args)
    {
        # si es un comando el que modifica no se tiene encuenta
        if ($this->isConsoleCommand()) {
            return;
        }
        # si la accion es de una ruta excluida como el login no se tiene encuenta
        $request = $this->requestStack->getCurrentRequest();
        if (in_array($request->attributes->get('_route'), static::EXCLUDEROUTES)) {
            return;
        }
        # si existe un usuario en sesion entonces
        if (
            ($user = $this->security->getUser())
            && ($entity = $args->getObject()) 
            && ($entityClass = get_class($entity))
            && $this->isRegistrationAllowed($entityClass)
        ) {
            $request = $this->requestStack->getCurrentRequest();
            $tableName = $this->getTableName($entityClass);
            if ($tableName) {
                $data = $this->getDataRequest($user, $tableName, $action, $this->getDataTable($entity, $tableName));
                $this->logger->register(
                    $data['userId'],
                    $request->getSession()->getId(), 
                    $this->appname, 
                    'databaseChange', 
                    $data, 
                    '103', 
                    DateSion::sum('day', 7)
                );
            }
        }
    }

    /**
     * Permite saber si la accion se realizó desde un comando
     */
    private function isConsoleCommand(): bool
    {
        return PHP_SAPI === 'cli';
    }

    /**
     * Permite saber si una entity se puede registrar en el log
     */
    private function isRegistrationAllowed(string $entityClass): bool
    {
        if ($this->strategy == 'allowlist') {
            return \in_array($entityClass, static::LISTENTITIES);
        } else {
            return !\in_array($entityClass, static::LISTENTITIES);
        }
    }

    /**
     * Devuelve el nombre de la tabla que representa la entity en la base de datos
     */
    private function getTableName(string $entityClass): ?string 
    {
        $metadata = $this->entityManager->getClassMetadata($entityClass);
        return $metadata->getTableName();
    }

    /**
     * Obtiene la data de un registro buscado por id en la tabla 
     */
    private function getDataTable($entity, string $tableName): array
    {
        $reflectionClass = new \ReflectionClass($entity);
        if ($reflectionClass->hasMethod('getId')) {
            $tableId = $entity->getId();
            return $this->sqlSion->fetchOne("SELECT * FROM {$tableName} WHERE id = '{$tableId}'");
        }
        return [];
    }

    /**
     * Obtener la data que se va a guardar en el log
     */
    private function getDataRequest(
        UserSession $user, 
        string $tableName, 
        string $action,
        array $tableData = null
    ): array {
        $request = $this->requestStack->getCurrentRequest();
        $personId = $user->getPersonId() ? $user->getPersonId() : '';
        return [
            'userId' => $user->getId(),
            'personId' => $personId,
            'ip' => ClientSion::ipClient(),
            'route' => $request->attributes->get('_route'),
            'method' => $request->getMethod(),
            'url' => $request->getPathInfo(),
            'table' => [
                'name' => $tableName,
                'date' => DateSion::date()->format("Y-m-d H:i:s"),
                'action' => $action,
                'data' => $tableData
            ]
        ];
    }
}