<?php
namespace App\Listener;

/**
 * Tuyos, oh Señor, son la grandeza, el poder, la gloria, la victoria y la majestad. 
 * Todo lo que hay en los cielos y en la tierra es tuyo, oh Señor, y este es tu reino. 
 * Te adoramos como el que está por sobre todas las cosas.
 * 1 Cronicas 29:11 NTV
 */

use App\Security\Exception\RedirectUrlException;
use App\Security\GeneratorAccessToken;
use Symfony\Component\Security\Http\Event\LoginSuccessEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\Request;

/**
 * Crea el AccessToken y carga la data del usuario en AWS-Dynamodb 
 * Tambien identificar el inicio de sesión para luego borrar el acctkn de la url
 * para proveer usuarios desde esta fuente
 *
 * @author Walther Bojaca <wbojaca@supresencia.com>
 */
class CustomLoginSuccessListener implements EventSubscriberInterface
{
    private $generatorToken;

    public function __construct(GeneratorAccessToken $generatorToken)
    {
        $this->generatorToken = $generatorToken;
    }

    /**
     * Devuelve la ruta de origen, si no existe va al homepage 
     */
    private function getTargetPath(Request $request): string
    {
        return $request->getSession()->get('_security.main.target_path') ?$request->getSession()->get('_security.main.target_path') : $request->getUri();
    }

    public static function getSubscribedEvents(): array
    {
        return [
            LoginSuccessEvent::class => 'onLoginSuccess',
        ];
    }

    public function onLoginSuccess(LoginSuccessEvent $event): void
    {
        if ('api' !== $event->getFirewallName()) {
            $event->getRequest()->getSession()->set('_syncSessionExpire', \date("Y-m-d H:i:s"));
            if ($accessToken = self::getAccessToken($event->getRequest()->getSession()->getId())) {
                $this->generatorToken->createAccessToken($accessToken, $event->getUser());
            }
            if ($event->getRequest()->query->has('acctkn')) {
                $event->getRequest()->query->remove('acctkn');
                $event->getRequest()->overrideGlobals();
                throw new RedirectUrlException($this->getTargetPath($event->getRequest()));
            }
        }
    }

    private static function getAccessToken(?string $sessionId): string
    {
        return $sessionId ? 'PHPSESSID_'. $sessionId : null; 
    }
}