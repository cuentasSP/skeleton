<?php
namespace App\Listener;

/**
 * Tuyos, oh Señor, son la grandeza, el poder, la gloria, la victoria y la majestad. 
 * Todo lo que hay en los cielos y en la tierra es tuyo, oh Señor, y este es tu reino. 
 * Te adoramos como el que está por sobre todas las cosas.
 * 1 Cronicas 29:11 NTV
 * 
 * ¡Glorifica al Señor, oh Jerusalén!
 *    ¡Alaba a tu Dios, oh Sion!
 *  Pues él ha reforzado las rejas de tus puertas
 *    y ha bendecido a tus hijos que habitan dentro de tus murallas.
 * Salmo 147:12-13 NTV
 */

use App\Security\Exception\RedirectUrlException;
use App\Service\Aws\DynamodbAws;
use App\Security\ProtectedRoutes;
use App\Security\UserSession;
use Sion\CryptoSion;
use Sion\DateSion;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\HttpKernel\Event\RequestEvent;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Bundle\SecurityBundle\Security;

/**
 * Para controlar si el acceso o la acción requiere autorización
 * @author Walther Bojacá <wbojaca@supresencia.com>
 */
class AuthorizedAccessListener
{
    private $request;
    private $security;
    private $dynamodbAws;
    private $urlGenerator;
    private $appname;
    private $authorizations;
    private $device;
    private $hostAccounts;
    private $keySecret;
    private $dynamoData;
    private $authorized = false;

    const TRUSTLEVEL = [
        'bajo' => 1,
        'medio' => 10,
        'alto' => 100
    ];

    public function __construct (
        RequestStack $requestStack, 
        Security $security,
        DynamodbAws $dynamodbAws, 
        UrlGeneratorInterface $urlGenerator,
        ParameterBagInterface $prms
    ) {
        $this->request = $requestStack;
        $this->security = $security;
        $this->dynamodbAws = $dynamodbAws;
        $this->urlGenerator = $urlGenerator;
        $this->appname = $prms->get('appname');
        $this->hostAccounts = $prms->get('host_accounts');
        $this->authorizations = $prms->get('tabla_authorizations');
        $this->device = $prms->get('tabla_device');
        $this->keySecret = $prms->get('key_secret');
        $this->dynamoData = $prms->get('dynamo_data');
    }

    /**
     * Devuelve la url del action para el formulario que procesa la acción una vez se autorice la acción.
     * Dependiendo si el environment es prod (producción) se puede generar la url relativa,
     * si es dev (desarrollo) o otro, entonces devolver la url absoluta para no tener problemas al procesar el formulario
     */
    private function getActionForm(Request $request): string
    {
        $environment = $request->server->get('APP_ENV');
        if ($environment == 'prod') {
            return $request->getPathInfo();
            //return str_replace('http://', 'https://', $request->getUri());
        }
        return $request->getUri();
    }

    /**
     * Devuelve el id de autorizacion
     */
    private function getAuthorizationCode(string $sessionId, string $id): string
    {
        return $this->appname.".{$id}_{$sessionId}";
    }

    /**
     * Devuelve el id de la sesión
     */
    private function session(): SessionInterface
    {
        return $this->request->getSession();
    }

    /**
     * Busca una autorizacion
     */
    private function getAuthorization(string $sessionId, string $id): array
    {
        $code = $this->getAuthorizationCode($sessionId, $id);
        return $this->dynamodbAws->getItem($this->authorizations, ['id' => ['S' => $code]], ['id']);
    }

    /**
     * Evaluar si el dispositivo tiene el nivel de confianza requerido
     */
    private function isTrustLevel(UserSession $user, string $trustLevel): bool
    {
        if (!$user->getDeviceId()) {
            return false;
        }
        $intTrustLevel = !empty(static::TRUSTLEVEL[$trustLevel]) ? static::TRUSTLEVEL[$trustLevel] : 100;
        $key = ['id' => ['S' => $user->getDeviceId()], 'user' => ['S' => ''.$user->getId()]];
        $device = $this->dynamodbAws->getItem($this->device, $key, ['id', 'user', 'trustLevel']);
        if (!empty($device['user']) && !empty($device['trustLevel'])) {
            $deviceTrustLevel = !empty(static::TRUSTLEVEL[$device['trustLevel']]) ? static::TRUSTLEVEL[$device['trustLevel']] : 1;
            return $device['user'] == $user->getId() && $deviceTrustLevel >= $intTrustLevel;
        }
        return false;
    }

    /**
     * Verificar si el usuario está autorizado para continuar
     * 1. Evaluar si la ruta protegida tiene trustLevel activado y si el dispositivo tiene el nivel requerido se autoriza
     * 2. Evaluar si el usuario cuenta con una autorización
     */
    private function isAuthorized(array $data): bool
    {
        # 1. Validar trustLevel
        if (!empty($data['trustLevel']) && $this->isTrustLevel($this->security->getUser(), $data['trustLevel'])) {
            $this->authorized = true;
        }
        # 2. Validar si el usuario tiene una autorización
        if (!$this->authorized) {
            $this->authorized = $this->getAuthorization($this->session()->getId(), $data['id']) ? true : false;
        }
        return $this->authorized;
    }
    
    /**
     * Devuelve la url con el _bearer necesario para solicitar una autorizacion a accounts
     */
    private function getUrlSeekAuthorization(string $sessionId, string $targetPath, array $data): string
    {
        $payload = [
            '_id' => $this->getAuthorizationCode($sessionId, $data['id']),
            '_target_path' => $targetPath,
            '_type' => $data['type'],
            '_expire' => !empty($data['ttl']) ? DateSion::sum('minutes', $data['ttl'])->format("Y-m-d H:i:00"): DateSion::sum('minutes', 15)->format("Y-m-d H:i:00")
        ];
        if (!empty($data['trustLevel'])) {
            $payload['_trustLevel'] = $data['trustLevel'];
        }
        if (!empty($data['blockLoginAs'])) {
            $payload['_block_login_as'] = true;
        }
        $bearer = CryptoSion::encrypt(\json_encode($payload), $this->keySecret);
        return $this->hostAccounts."/security/seek-authorization?_bearer={$bearer['encrypted']}.{$bearer['nonce']}";
    }

    /**
     * Guarda la data de formulario que se desea procesar para recuperarlos despues de tener la autorización
     */
    private function setDataActionUser(string $sessionId, array $data): void
    {
        $expire = DateSion::sum('horas', 12, null)->getTimestamp();
        $data = [
            'id'=>        ['S'=> $sessionId],
            'topic'=>     ['S'=> 'verificationCode'],
            'data'=>      ['S'=> \json_encode($data)],
            'date'=>      ['S'=> date("Y-m-d")],
            'datetime'=>  ['S'=> date("Y-m-d H:i:s")],
            'expire'=>    ['N' => ''.$expire]
        ];
        $this->dynamodbAws->setItem($this->dynamoData, $data);
    }

    /**
     * Evalua si la petición correspende a una protegida que requiere autorización
     */
    public function onKernelRequest(RequestEvent $event): void
    {
        $request = $event->getRequest();
        $route = $request->attributes->get('_route');
        // Si la ruta a la que se quiere acceder (GET) requiere autorización y aun no está autorizado
        if ($request->getMethod() == 'GET' && !empty(ProtectedRoutes::_PROTECTED_ACCESS["{$route}"]) && !$this->isAuthorized(ProtectedRoutes::_PROTECTED_ACCESS["{$route}"])) {
            throw new RedirectUrlException( $this->getUrlSeekAuthorization($request->getSession()->getId(), $request->getUri(), ProtectedRoutes::_PROTECTED_ACCESS["{$route}"]) );
        }
        // Si la acción (POST) require autorización y aun no tiene, antes de ser procesada
        if ($request->getMethod() == 'POST' && !empty(ProtectedRoutes::_PROTECTED_ACTION["{$route}"]) && !$this->isAuthorized(ProtectedRoutes::_PROTECTED_ACTION["{$route}"])) {
            $data = [
                'authorizationCode' => $this->getAuthorizationCode($request->getSession()->getId(), ProtectedRoutes::_PROTECTED_ACTION["{$route}"]['id']),
                'action' => $this->getActionForm($request),
                'data' => $request->request->all()
            ];
            $this->setDataActionUser($this->session()->getId(), $data);
            throw new RedirectUrlException( $this->getUrlSeekAuthorization(
                $request->getSession()->getId(), 
                $this->urlGenerator->generate('app_continue_action_user', [], UrlGeneratorInterface::ABSOLUTE_URL), 
                ProtectedRoutes::_PROTECTED_ACTION["{$route}"]) 
            );
        }
    }
}
