<?php
namespace App\Controller;

/**
 * El Señor dice a Zorobabel: 
 * No es por el poder ni por la fuerza, sino por mi Espíritu, dice el Señor de los Ejércitos Celestiales. 
 *  Nada impedirá el camino de Zorobabel, ni siquiera una montaña gigantesca, 
 *    ¡pues se convertirá en llanura delante de él! 
 * Y cuando Zorobabel coloque la última piedra del templo en su lugar, la gente gritará: 
 *   "¡Dios lo bendiga! ¡Dios lo bendiga!"
 * Zacarías 4:6-7 NTV
 */

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Uid\Uuid;

class HomeController extends AbstractController
{
    #[Route(path: '/', name: 'app_homepage', methods: ['GET'])]
    public function index(): Response
    {
        return $this->render('home/index.html.twig', [
            'controller_name' => 'HomeController',
        ]);
    }

    #[Route(path: '/test', name: 'app_test', methods: ['GET'])]
    public function test(): Response
    {
        $data = [
            'uuid' => Uuid::v4()->toRfc4122()
        ];
        return $this->render('home/test.html.twig', $data);
    }

    #[Route(path: '/test-form', name: 'app_test_form', methods: ['GET', 'POST'])]
    public function testForm(): Response
    {
        return $this->render('home/test_form.html.twig');
    }
}
