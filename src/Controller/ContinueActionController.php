<?php
namespace App\Controller;

use App\Service\Aws\DynamodbAws;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Permite continuar con la acción que estaba realizando un usuario cuando se solicitó autorización  
 * @author Walther Bojacá <wbojaca@supresencia.com>
 */
class ContinueActionController extends AbstractController
{
    private $request;
    private $dynamodbAws;
    private $authorizations;
    private $dynamoData;

    public function __construct(
        RequestStack $request, 
        DynamodbAws $dynamodbAws, 
        ParameterBagInterface $prms
    ) {
        $this->request = $request;
        $this->dynamodbAws = $dynamodbAws;
        $this->authorizations = $prms->get('tabla_authorizations');
        $this->dynamoData = $prms->get('dynamo_data');
    }

    private function getAuthorization(string $authorizationCode): array
    {
        return $this->dynamodbAws->getItem($this->authorizations, ['id' => ['S' => $authorizationCode]], ['id', 'dataUser']);
    }

    private function getDataActionDynamo(string $sessionId): array
    {
        $key = [
            'id'=>        ['S'=> $sessionId],
            'topic'=>     ['S'=> 'verificationCode']
        ];
        if ($data = $this->dynamodbAws->getItem($this->dynamoData, $key, ['id','topic','data'])) {
            return \json_decode($data['data'], true);
        }
        return [];
    }

    private function getDataAction(): array
    {
        $sessionId = $this->request->getSession()->getId();
        $dataAction = $this->getDataActionDynamo($sessionId);
        if (
            !empty($dataAction['authorizationCode'])
            && !empty($dataAction['action'])
            && !empty($dataAction['data'])
            && ($this->getAuthorization($dataAction['authorizationCode']))
        ) {
            return $dataAction;
        }
        return [];
    }

    /**
     * Agrega al Response cabeceras para no permitir guardar en cache el sitio
     */
    private function responseNoCache(Response $response): Response
    {
        $response->headers->set('Cache-Control', 'no-store, no-cache, must-revalidate, max-age=0');
        $response->headers->set('Pragma', 'no-cache');
        return $response;
    }

    #[Route("/continue-action-user", name:"app_continue_action_user")]
    public function __invoke(): Response
    {
        if ($dataAction = $this->getDataAction()) {
            return $this->responseNoCache($this->render('security/continue_action.html.twig', [
                'form' => $dataAction
            ]));
        }
        return new Response('', 404);
    }
}
