<?php
namespace App\Controller;

/**
 * »Te he dado a conocer a los que me diste de este mundo. 
 * Siempre fueron tuyos. Tú me los diste, y ellos han obedecido tu palabra. 
 * Ahora saben que todo lo que tengo es un regalo que proviene de ti, 
 * porque les he transmitido el mensaje que me diste. 
 * Ellos aceptaron el mensaje y saben que provine de ti y han creído 
 * que tú me enviaste.
 * Juan 17:6-8
 */

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

/**
 * Controlador para manejar el inicio de sesión basado en supresencia/accounts
 * @author Walther Bojacá <wbojaca@supresencia.com>
 */
class SigninController extends AbstractController
{
    /**
     * Forzar crear una sesion si no hay una creada
     */
    private function startSession(Request $request): void
    {
        if (!$request->getSession()->get('startSession')) {
            $request->getSession()->set('startSession', \date("Y-m-d H:i:s"));
        }
    }

    private function getTargetPath(?string $targetPath): string
    {
        return $targetPath ?: $this->generateUrl('app_homepage', [], UrlGeneratorInterface::ABSOLUTE_URL);
    }

    #[Route("/signin", name:"app_signin")]
    public function index(Request $request): Response
    {
        $this->startSession($request);
        # obtener el targetPath para redirigir cuando el usuario sea autenticado
        $targetPath = \urlencode($this->getTargetPath($request->getSession()->get('_security.main.target_path')));
        return new RedirectResponse($this->getParameter('host_accounts')."/signin?_target_path=$targetPath");
    }

    #[Route("/logout", name:"app_logout", methods:["GET"])]
    public function logout()
    {
        throw new \Exception('Don\'t forget to activate logout in security.yaml');
    }
}
