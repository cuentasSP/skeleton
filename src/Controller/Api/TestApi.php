<?php
namespace App\Controller\Api;

/**
 * Pero tú, Daniel, mantén en secreto esta profecía; sella el libro hasta el tiempo del fin, 
 * cuando muchos correrán de aquí para allá y el conocimiento aumentará.
 * Daniel 12:4 NTV
 */

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Controlador de ejemplo para desarrollar api
 * @author Walther Bojacá <wbojaca@supresencia.com>
 */
#[Route("/api", name:"api_test_")]
class TestApi extends AbstractController
{
    #[Route("/test", name:"test")]
    public function index(): JsonResponse
    {
        return new JsonResponse(['status' => true]);
    }
}
