<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class TestController extends AbstractController
{
    #[Route('/test', name: 'app_test')]
    public function index(): Response
    {
        return $this->render('test/index.html.twig');
    }

    #[Route('/test-form', name: 'app_test_form')]
    public function testForm(Request $request): Response
    {
        return $this->render('test/form.html.twig', [
            'data' => $request->request->all()
        ]);
    }
}
