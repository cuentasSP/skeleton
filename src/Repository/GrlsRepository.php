<?php
namespace App\Repository;

use App\Entity\GrlsEstados;
use App\Entity\GrlsTipos;
use App\Entity\GrlsTiposGrupos;
use Sion\EntitySion;

class GrlsRepository
{
    private $entity;

    public function __construct(EntitySion $entity)
    {
        $this->entity = $entity;
    }

    public function status(array $by): ?GrlsEstados
    {
        return $this->entity->findOneBy(GrlsEstados::class, $by);
    }
    
    public function type(array $by): ?GrlsTipos
    {
        return $this->entity->findOneBy(GrlsTipos::class, $by);
    }

    public function groupType(array $by): ?GrlsTiposGrupos
    {
        return $this->entity->findOneBy(GrlsTiposGrupos::class, $by);
    }
}