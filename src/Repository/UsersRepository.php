<?php
namespace App\Repository;

use App\Entity\AccUsuarios;
use Sion\EntitySion;

class UsersRepository
{
    private $entity;

    public function __construct(EntitySion $entity)
    {
        $this->entity = $entity;
    }

    public function user(string $id): ?AccUsuarios
    {
        return $this->entity->find(AccUsuarios::class, $id);
    }
    
    public function userBy(array $by): ?AccUsuarios
    {
        return $this->entity->findOneBy(AccUsuarios::class, $by);
    }
}