<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * GrlsEstados
 */
#[ORM\Table(name: 'grls_estados')]
#[ORM\Entity]
class GrlsEstados
{
    /**
     * @var int
     */
    #[ORM\Column(name: 'id', type: 'integer', nullable: false)]
    #[ORM\Id]
    #[ORM\GeneratedValue(strategy: 'IDENTITY')]
    private $id;

    /**
     * @var string|null
     */
    #[ORM\Column(name: 'descripcion', type: 'string', length: 50, nullable: true)]
    private $descripcion;

    /**
     * @var string|null
     */
    #[ORM\Column(name: 'tipo', type: 'string', length: 50, nullable: true)]
    private $tipo;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getDescripcion(): ?string
    {
        return $this->descripcion;
    }

    public function setDescripcion(?string $descripcion): self
    {
        $this->descripcion = $descripcion;

        return $this;
    }

    public function getTipo(): ?string
    {
        return $this->tipo;
    }

    public function setTipo(?string $tipo): self
    {
        $this->tipo = $tipo;

        return $this;
    }


}
