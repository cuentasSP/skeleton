<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * GrlsTipos
 */
#[ORM\Table(name: 'grls_tipos')]
#[ORM\Index(name: 'FK_grl_tipos_grl_tipos_grupos', columns: ['id_grupo'])]
#[ORM\Index(name: 'FK_grl_tipos_grl_estados', columns: ['estado'])]
#[ORM\Entity]
class GrlsTipos
{
    /**
     * @var int
     */
    #[ORM\Column(name: 'id', type: 'integer', nullable: false)]
    #[ORM\Id]
    #[ORM\GeneratedValue(strategy: 'IDENTITY')]
    private $id;

    /**
     * @var string|null
     */
    #[ORM\Column(name: 'descripcion', type: 'string', length: 50, nullable: true)]
    private $descripcion;

    /**
     * @var \GrlsEstados
     */
    #[ORM\JoinColumn(name: 'estado', referencedColumnName: 'id')]
    #[ORM\ManyToOne(targetEntity: \GrlsEstados::class)]
    private $estado;

    /**
     * @var \GrlsTiposGrupos
     */
    #[ORM\JoinColumn(name: 'id_grupo', referencedColumnName: 'id')]
    #[ORM\ManyToOne(targetEntity: \GrlsTiposGrupos::class)]
    private $idGrupo;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getDescripcion(): ?string
    {
        return $this->descripcion;
    }

    public function setDescripcion(?string $descripcion): self
    {
        $this->descripcion = $descripcion;

        return $this;
    }

    public function getEstado(): ?GrlsEstados
    {
        return $this->estado;
    }

    public function setEstado(?GrlsEstados $estado): self
    {
        $this->estado = $estado;

        return $this;
    }

    public function getIdGrupo(): ?GrlsTiposGrupos
    {
        return $this->idGrupo;
    }

    public function setIdGrupo(?GrlsTiposGrupos $idGrupo): self
    {
        $this->idGrupo = $idGrupo;

        return $this;
    }


}
