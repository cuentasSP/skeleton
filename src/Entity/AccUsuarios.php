<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * AccUsuarios
 */
#[ORM\Table(name: 'acc_usuarios')]
#[ORM\Index(name: 'IDX_FDFB7F7E8F781FEB', columns: ['id_persona'])]
#[ORM\Index(name: 'FK_acc_usuarios_grls_estados', columns: ['estado'])]
#[ORM\UniqueConstraint(name: 'UNIQ_FDFB7F7E92FC23A8', columns: ['username_canonical'])]
#[ORM\UniqueConstraint(name: 'UNIQ_FDFB7F7EA0D96FBF', columns: ['email_canonical'])]
#[ORM\Entity]
class AccUsuarios 
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private $id;

    #[ORM\Column(name: 'hash', type: 'string', length: 50, nullable: true)]
    private $hash;

    #[ORM\Column(name: 'fecha', type: 'datetime', nullable: true)]
    private $fecha;

    #[ORM\Column(type: 'string', length: 180, unique: true)]
    private $username;

    #[ORM\Column(name: 'username_canonical', type: 'string', length: 180, nullable: false)]
    private $usernameCanonical;

    #[ORM\Column(name: 'email', type: 'string', length: 255, nullable: false)]
    private $email;

    #[ORM\Column(name: 'email_canonical', type: 'string', length: 255, nullable: false)]
    private $emailCanonical;

    #[ORM\Column(type: 'array')]
    protected $roles = [];

    /**
     * @var string The hashed password
     */
    #[ORM\Column(type: 'string')]
    private $password;

    /**
     * @var string
     */
    #[ORM\Column(name: 'salt', type: 'string', length: 255, nullable: false)]
    private $salt;

    /**
     * @var \DateTime|null
     */
    #[ORM\Column(name: 'last_login', type: 'datetime', nullable: true)]
    private $lastLogin;

    #[ORM\Column(name: 'enabled', type: 'boolean', nullable: false)]
    private $enabled;

    /**
     * @var \CtaPersonas
     */
    #[ORM\JoinColumn(name: 'id_persona', referencedColumnName: 'id')]
    #[ORM\ManyToOne(targetEntity: \CtaPersonas::class)]
    private $idPersona;

    /**
     * @var \GrlsEstados
     */
    #[ORM\JoinColumn(name: 'estado', referencedColumnName: 'id')]
    #[ORM\ManyToOne(targetEntity: \GrlsEstados::class)]
    private $estado;


    public function __construct()
    {
        $this->fecha = new \DateTime(date("Y-m-d H:i:s"));
        $this->hash = sha1('usersp.'.uniqid());
        $this->salt = base_convert(sha1(uniqid(mt_rand(), true)), 16, 36);
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getHash(): ?string
    {
        return $this->hash;
    }

    public function setHash(?string $hash): self
    {
        $this->hash = $hash;

        return $this;
    }

    public function getUsername(): string
    {
        return (string) $this->username;
    }

    public function setUsername(string $username): self
    {
        $this->username = $username;

        return $this;
    }

    public function getRoles(): array
    {
        $roles = $this->roles;
        // guarantee every user at least has ROLE_USER
        $roles[] = 'ROLE_USER';

        return array_unique($roles);
    }

    public function setRoles(array $roles): self
    {
        $this->roles = $roles;

        return $this;
    }

    public function getPassword(): string
    {
        return (string) $this->password;
    }

    public function setPassword(string $password): self
    {
        $this->password = $password;

        return $this;
    }

    public function getSalt(): ?string
    {
        return $this->salt;
    }

    public function setSalt(string $salt): self
    {
        $this->salt = $salt;
        return $this;
    }

    public function getLastLogin(): ?\DateTimeInterface
    {
        return $this->lastLogin;
    }

    public function setLastLogin(?\DateTimeInterface $lastLogin): self
    {
        $this->lastLogin = $lastLogin;

        return $this;
    }

    public function getIdPersona(): ?CtaPersonas
    {
        return $this->idPersona;
    }

    public function setIdPersona(?CtaPersonas $idPersona): self
    {
        $this->idPersona = $idPersona;

        return $this;
    }

    public function getEstado(): ?GrlsEstados
    {
        return $this->estado;
    }

    public function setEstado(?GrlsEstados $estado): self
    {
        $this->estado = $estado;

        return $this;
    }

    public function getUsernameCanonical(): ?string
    {
        return $this->usernameCanonical;
    }

    public function setUsernameCanonical(string $usernameCanonical): self
    {
        $this->usernameCanonical = $usernameCanonical;

        return $this;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    public function getEmailCanonical(): ?string
    {
        return $this->emailCanonical;
    }

    public function setEmailCanonical(string $emailCanonical): self
    {
        $this->emailCanonical = $emailCanonical;

        return $this;
    }

    public function getEnabled(): ?bool
    {
        return $this->enabled;
    }

    public function setEnabled(bool $enabled): self
    {
        $this->enabled = $enabled;

        return $this;
    }

    public function __toString()
    {
        return 'AccUsuarios';
    }
}
