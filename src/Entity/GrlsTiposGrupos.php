<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * GrlsTiposGrupos
 */
#[ORM\Table(name: 'grls_tipos_grupos')]
#[ORM\Index(name: 'FK__grl_estados', columns: ['estado'])]
#[ORM\Entity]
class GrlsTiposGrupos
{
    /**
     * @var int
     */
    #[ORM\Column(name: 'id', type: 'integer', nullable: false)]
    #[ORM\Id]
    #[ORM\GeneratedValue(strategy: 'IDENTITY')]
    private $id;

    /**
     * @var string|null
     */
    #[ORM\Column(name: 'descripcion', type: 'string', length: 50, nullable: true)]
    private $descripcion;

    /**
     * @var \GrlsEstados
     */
    #[ORM\JoinColumn(name: 'estado', referencedColumnName: 'id')]
    #[ORM\ManyToOne(targetEntity: \GrlsEstados::class)]
    private $estado;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getDescripcion(): ?string
    {
        return $this->descripcion;
    }

    public function setDescripcion(?string $descripcion): self
    {
        $this->descripcion = $descripcion;

        return $this;
    }

    public function getEstado(): ?GrlsEstados
    {
        return $this->estado;
    }

    public function setEstado(?GrlsEstados $estado): self
    {
        $this->estado = $estado;

        return $this;
    }


}
