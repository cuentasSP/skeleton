<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * GrlsPaises
 */
#[ORM\Table(name: 'grls_paises')]
#[ORM\Index(name: 'ISO2', columns: ['ISO2'])]
#[ORM\Index(name: 'ISO3', columns: ['ISO3'])]
#[ORM\Index(name: 'FK_grl_paises_grl_estados', columns: ['estado'])]
#[ORM\Entity]
class GrlsPaises
{
    /**
     * @var int
     */
    #[ORM\Column(name: 'id', type: 'integer', nullable: false)]
    #[ORM\Id]
    #[ORM\GeneratedValue(strategy: 'IDENTITY')]
    private $id;

    /**
     * @var string|null
     */
    #[ORM\Column(name: 'pais', type: 'string', length: 100, nullable: true)]
    private $pais;

    /**
     * @var string|null
     */
    #[ORM\Column(name: 'es', type: 'string', length: 100, nullable: true)]
    private $es;

    /**
     * @var string|null
     */
    #[ORM\Column(name: 'en', type: 'string', length: 100, nullable: true)]
    private $en;

    /**
     * @var string|null
     */
    #[ORM\Column(name: 'FIPS104', type: 'string', length: 5, nullable: true)]
    private $fips104;

    /**
     * @var string|null
     */
    #[ORM\Column(name: 'ISO2', type: 'string', length: 5, nullable: true)]
    private $iso2;

    /**
     * @var string|null
     */
    #[ORM\Column(name: 'ISO3', type: 'string', length: 5, nullable: true)]
    private $iso3;

    /**
     * @var string|null
     */
    #[ORM\Column(name: 'ISON', type: 'string', length: 5, nullable: true)]
    private $ison;

    /**
     * @var string|null
     */
    #[ORM\Column(name: 'internet', type: 'string', length: 5, nullable: true)]
    private $internet;

    /**
     * @var string|null
     */
    #[ORM\Column(name: 'capital', type: 'string', length: 100, nullable: true)]
    private $capital;

    /**
     * @var string|null
     */
    #[ORM\Column(name: 'map_reference', type: 'string', length: 100, nullable: true)]
    private $mapReference;

    /**
     * @var string|null
     */
    #[ORM\Column(name: 'nationality_singular', type: 'string', length: 100, nullable: true)]
    private $nationalitySingular;

    /**
     * @var string|null
     */
    #[ORM\Column(name: 'nationality_plural', type: 'string', length: 100, nullable: true)]
    private $nationalityPlural;

    /**
     * @var string|null
     */
    #[ORM\Column(name: 'moneda', type: 'string', length: 100, nullable: true)]
    private $moneda;

    /**
     * @var string|null
     */
    #[ORM\Column(name: 'cod_moneda', type: 'string', length: 10, nullable: true)]
    private $codMoneda;

    /**
     * @var string|null
     */
    #[ORM\Column(name: 'population', type: 'string', length: 100, nullable: true)]
    private $population;

    /**
     * @var string|null
     */
    #[ORM\Column(name: 'title', type: 'string', length: 100, nullable: true)]
    private $title;

    /**
     * @var string|null
     */
    #[ORM\Column(name: 'prefijo_tel', type: 'string', length: 5, nullable: true)]
    private $prefijoTel;

    /**
     * @var \GrlsEstados
     */
    #[ORM\JoinColumn(name: 'estado', referencedColumnName: 'id')]
    #[ORM\ManyToOne(targetEntity: \GrlsEstados::class)]
    private $estado;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getPais(): ?string
    {
        return $this->pais;
    }

    public function setPais(?string $pais): self
    {
        $this->pais = $pais;

        return $this;
    }

    public function getEs(): ?string
    {
        return $this->es;
    }

    public function setEs(?string $es): self
    {
        $this->es = $es;

        return $this;
    }

    public function getEn(): ?string
    {
        return $this->en;
    }

    public function setEn(?string $en): self
    {
        $this->en = $en;

        return $this;
    }

    public function getFips104(): ?string
    {
        return $this->fips104;
    }

    public function setFips104(?string $fips104): self
    {
        $this->fips104 = $fips104;

        return $this;
    }

    public function getIso2(): ?string
    {
        return $this->iso2;
    }

    public function setIso2(?string $iso2): self
    {
        $this->iso2 = $iso2;

        return $this;
    }

    public function getIso3(): ?string
    {
        return $this->iso3;
    }

    public function setIso3(?string $iso3): self
    {
        $this->iso3 = $iso3;

        return $this;
    }

    public function getIson(): ?string
    {
        return $this->ison;
    }

    public function setIson(?string $ison): self
    {
        $this->ison = $ison;

        return $this;
    }

    public function getInternet(): ?string
    {
        return $this->internet;
    }

    public function setInternet(?string $internet): self
    {
        $this->internet = $internet;

        return $this;
    }

    public function getCapital(): ?string
    {
        return $this->capital;
    }

    public function setCapital(?string $capital): self
    {
        $this->capital = $capital;

        return $this;
    }

    public function getMapReference(): ?string
    {
        return $this->mapReference;
    }

    public function setMapReference(?string $mapReference): self
    {
        $this->mapReference = $mapReference;

        return $this;
    }

    public function getNationalitySingular(): ?string
    {
        return $this->nationalitySingular;
    }

    public function setNationalitySingular(?string $nationalitySingular): self
    {
        $this->nationalitySingular = $nationalitySingular;

        return $this;
    }

    public function getNationalityPlural(): ?string
    {
        return $this->nationalityPlural;
    }

    public function setNationalityPlural(?string $nationalityPlural): self
    {
        $this->nationalityPlural = $nationalityPlural;

        return $this;
    }

    public function getMoneda(): ?string
    {
        return $this->moneda;
    }

    public function setMoneda(?string $moneda): self
    {
        $this->moneda = $moneda;

        return $this;
    }

    public function getCodMoneda(): ?string
    {
        return $this->codMoneda;
    }

    public function setCodMoneda(?string $codMoneda): self
    {
        $this->codMoneda = $codMoneda;

        return $this;
    }

    public function getPopulation(): ?string
    {
        return $this->population;
    }

    public function setPopulation(?string $population): self
    {
        $this->population = $population;

        return $this;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(?string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function getPrefijoTel(): ?string
    {
        return $this->prefijoTel;
    }

    public function setPrefijoTel(?string $prefijoTel): self
    {
        $this->prefijoTel = $prefijoTel;

        return $this;
    }

    public function getEstado(): ?GrlsEstados
    {
        return $this->estado;
    }

    public function setEstado(?GrlsEstados $estado): self
    {
        $this->estado = $estado;

        return $this;
    }


}
