<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * CtaPersonas
 */
#[ORM\Table(name: 'cta_personas')]
#[ORM\Index(name: 'FK_cta_personas_grl_paises', columns: ['pais_origen'])]
#[ORM\Index(name: 'FK_cta_personas_acc_usuarios', columns: ['usuario'])]
#[ORM\Index(name: 'str_roles', columns: ['str_roles'])]
#[ORM\Index(name: 'estado', columns: ['estado'])]
#[ORM\Index(name: 'hash_bio', columns: ['hash_bio'])]
#[ORM\Index(name: 'nombre_apellidos', columns: ['nombre', 'apellidos'])]
#[ORM\UniqueConstraint(name: 'email', columns: ['email'])]
#[ORM\UniqueConstraint(name: 'nid', columns: ['nid'])]
#[ORM\UniqueConstraint(name: 'telefono', columns: ['telefono'])]
#[ORM\UniqueConstraint(name: 'hash', columns: ['hash'])]
#[ORM\Entity]
class CtaPersonas
{
    /**
     * @var int
     */
    #[ORM\Column(name: 'id', type: 'integer', nullable: false)]
    #[ORM\Id]
    #[ORM\GeneratedValue(strategy: 'IDENTITY')]
    private $id;

    /**
     * @var \DateTime|null
     */
    #[ORM\Column(name: 'fecha', type: 'datetime', nullable: true)]
    private $fecha;

    /**
     * @var string|null
     */
    #[ORM\Column(name: 'hash', type: 'string', length: 50, nullable: true)]
    private $hash;

    /**
     * @var string|null
     */
    #[ORM\Column(name: 'hash_bio', type: 'string', length: 50, nullable: true, options: ['comment' => 'sha1 nombre,apellidos y fechaNacimiento'])]
    private $hashBio;

    /**
     * @var string|null
     */
    #[ORM\Column(name: 'nombre', type: 'string', length: 100, nullable: true)]
    private $nombre;

    /**
     * @var string|null
     */
    #[ORM\Column(name: 'apellidos', type: 'string', length: 100, nullable: true)]
    private $apellidos;

    /**
     * @var string|null
     */
    #[ORM\Column(name: 'genero', type: 'string', length: 1, nullable: true)]
    private $genero;

    /**
     * @var \DateTime|null
     */
    #[ORM\Column(name: 'fecha_nacimiento', type: 'date', nullable: true)]
    private $fechaNacimiento;

    /**
     * @var int|null
     */
    #[ORM\Column(name: 'editar', type: 'smallint', nullable: true, options: ['default' => '1', 'comment' => 'si se puede o no editar (1: libre para editar, 2: editado por el usuario, 3: verificado y bloqueado para editar)'])]
    private $editar = '1';

    /**
     * @var string|null
     */
    #[ORM\Column(name: 'nid', type: 'string', length: 15, nullable: true, options: ['comment' => 'numero de identificacion'])]
    private $nid;

    /**
     * @var string|null
     */
    #[ORM\Column(name: 'telefono', type: 'string', length: 15, nullable: true, options: ['comment' => 'telefono unico para identificacion'])]
    private $telefono;

    /**
     * @var string|null
     */
    #[ORM\Column(name: 'email', type: 'string', length: 70, nullable: true, options: ['comment' => 'email unico para identificacion'])]
    private $email;

    /**
     * @var \DateTime|null
     */
    #[ORM\Column(name: 'update_at', type: 'datetime', nullable: true)]
    private $updateAt;

    /**
     * @var string|null
     */
    #[ORM\Column(name: 'str_roles', type: 'text', length: 65535, nullable: true)]
    private $strRoles;

    /**
     * @var \GrlsEstados
     */
    #[ORM\JoinColumn(name: 'estado', referencedColumnName: 'id')]
    #[ORM\ManyToOne(targetEntity: \GrlsEstados::class)]
    private $estado;

    /**
     * @var \GrlsPaises
     */
    #[ORM\JoinColumn(name: 'pais_origen', referencedColumnName: 'id')]
    #[ORM\ManyToOne(targetEntity: \GrlsPaises::class)]
    private $paisOrigen;

    /**
     * @var \AccUsuarios
     */
    #[ORM\JoinColumn(name: 'usuario', referencedColumnName: 'id')]
    #[ORM\ManyToOne(targetEntity: \AccUsuarios::class)]
    private $usuario;

    public function __construct() {
        $this->fecha = new \DateTime(date("Y-m-d H:i:s"));
        $this->hash = sha1('ammi.'.uniqid());
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getFecha(): ?\DateTimeInterface
    {
        return $this->fecha;
    }

    public function setFecha(?\DateTimeInterface $fecha): self
    {
        $this->fecha = $fecha;

        return $this;
    }

    public function getHash(): ?string
    {
        return $this->hash;
    }

    public function setHash(?string $hash): self
    {
        $this->hash = $hash;

        return $this;
    }

    public function getHashBio(): ?string
    {
        return $this->hashBio;
    }

    public function setHashBio(?string $hashBio): self
    {
        $this->hashBio = $hashBio;

        return $this;
    }

    public function getNombre(): ?string
    {
        return $this->nombre;
    }

    public function setNombre(?string $nombre): self
    {
        $this->nombre = $nombre;

        return $this;
    }

    public function getApellidos(): ?string
    {
        return $this->apellidos;
    }

    public function setApellidos(?string $apellidos): self
    {
        $this->apellidos = $apellidos;

        return $this;
    }

    public function getGenero(): ?string
    {
        return $this->genero;
    }

    public function setGenero(?string $genero): self
    {
        $this->genero = $genero;

        return $this;
    }

    public function getFechaNacimiento(): ?\DateTimeInterface
    {
        return $this->fechaNacimiento;
    }

    public function setFechaNacimiento(?\DateTimeInterface $fechaNacimiento): self
    {
        $this->fechaNacimiento = $fechaNacimiento;

        return $this;
    }

    public function getEditar(): ?int
    {
        return $this->editar;
    }

    public function setEditar(?int $editar): self
    {
        $this->editar = $editar;

        return $this;
    }

    public function getNid(): ?string
    {
        return $this->nid;
    }

    public function setNid(?string $nid): self
    {
        $this->nid = $nid;

        return $this;
    }

    public function getTelefono(): ?string
    {
        return $this->telefono;
    }

    public function setTelefono(?string $telefono): self
    {
        # obtener los últimos 10 o menos dígitos númericos del teléfono
        $sbtel = substr(preg_replace('/[^0-9]+/','',$telefono), -10);
        if(strlen($sbtel) > 5){
            $this->telefono = $sbtel;
        }
        return $this;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(?string $email): self
    {
        $this->email = $email;

        return $this;
    }

    public function getUpdateAt(): ?\DateTimeInterface
    {
        return $this->updateAt;
    }

    public function setUpdateAt(?\DateTimeInterface $updateAt): self
    {
        $this->updateAt = $updateAt;

        return $this;
    }

    public function getStrRoles(): ?string
    {
        return $this->strRoles;
    }

    public function setStrRoles(?string $strRoles): self
    {
        $this->strRoles = $strRoles;

        return $this;
    }

    public function getEstado(): ?GrlsEstados
    {
        return $this->estado;
    }

    public function setEstado(?GrlsEstados $estado): self
    {
        $this->estado = $estado;

        return $this;
    }

    public function getPaisOrigen(): ?GrlsPaises
    {
        return $this->paisOrigen;
    }

    public function setPaisOrigen(?GrlsPaises $paisOrigen): self
    {
        $this->paisOrigen = $paisOrigen;

        return $this;
    }

    public function getUsuario(): ?AccUsuarios
    {
        return $this->usuario;
    }

    public function setUsuario(?AccUsuarios $usuario): self
    {
        $this->usuario = $usuario;

        return $this;
    }


}
